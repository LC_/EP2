package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Locale;

import javax.swing.JFrame;
import javax.swing.JTextField;

import Model.calculosSimulacaoFluxoPotenciaFundamental;
import View.GraficosSimulacaoFPF;
import View.TrianguloDePotencias;

public class simularFluxoDePotenciaFundamental implements ActionListener {
	
															   //atributo agregacao			
	private calculosSimulacaoFluxoPotenciaFundamental simular; //atributo composicao
	private List<Double> scoresPotenciaInstantanea;
	private GraficosSimulacaoFPF graphPanelGraficoPotenciaInstantanea;
	private TrianguloDePotencias trianguloPotencias;
	private JTextField textFieldAmplitudeVrms;
	private JTextField textFieldAnguloTensao;
	private JTextField textFieldAmplitudeIrms;
	private JTextField textFieldAnguloCorrente;
	private JTextField textFieldPotenciaAtiva;
	private JTextField textFieldPotenciaReativa;
	private JTextField textFieldPotenciaAparente;
	private JTextField textFieldFatorDePotencia;
	 
	public simularFluxoDePotenciaFundamental( List<Double> scoresPotenciaInstantanea, GraficosSimulacaoFPF graphPanelGraficoPotenciaInstantanea, TrianguloDePotencias trianguloPotencias, JTextField textFieldAmplitudeVrms, JTextField textFieldAnguloTensao, JTextField textFieldAmplitudeIrms, JTextField textFieldAnguloCorrente, JTextField textFieldPotenciaAtiva, JTextField textFieldPotenciaReativa, JTextField textFieldPotenciaAparente, JTextField textFieldFatorDePotencia) {
		
		
		this.scoresPotenciaInstantanea=scoresPotenciaInstantanea;
		this.graphPanelGraficoPotenciaInstantanea=graphPanelGraficoPotenciaInstantanea;
		this.trianguloPotencias=trianguloPotencias;
		this.textFieldAmplitudeVrms=textFieldAmplitudeVrms;
		this.textFieldAnguloTensao=textFieldAnguloTensao;
		this.textFieldAmplitudeIrms=textFieldAmplitudeIrms;
		this.textFieldAnguloCorrente=textFieldAnguloCorrente;
		this.textFieldPotenciaAtiva=textFieldPotenciaAtiva;
		this.textFieldPotenciaReativa=textFieldPotenciaReativa;
		this.textFieldPotenciaAparente=textFieldPotenciaAparente;
		this.textFieldFatorDePotencia=textFieldFatorDePotencia;
		simular = new calculosSimulacaoFluxoPotenciaFundamental();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
				
		//setters das amplitudes e dos angulos 		
		simular.setAmplituteTensao(Double.parseDouble(textFieldAmplitudeVrms.getText()));
		simular.setAnguloTensao(Double.parseDouble(textFieldAnguloTensao.getText()) * (Math.PI/180));
		simular.setAmplitudeCorrente(Double.parseDouble(textFieldAmplitudeIrms.getText()));
		simular.setAnguloCorrente(Double.parseDouble(textFieldAnguloCorrente.getText()) * (Math.PI/180)) ;
		
		//setText dos calculos da potencia ativa
		textFieldPotenciaAtiva.setText(String.valueOf(Math.round(simular.calculaPotenciaAtiva()) + " W"));
		
		//setText dos calculos da potencia reativa
		textFieldPotenciaReativa.setText(String.valueOf(Math.round(simular.calculaPotenciaReativa()) + " VAR"));
	
		//setText dos calculos da potencia aparente
		textFieldPotenciaAparente.setText(String.valueOf(Math.round(simular.calculaPotenciaAparente()) + " VA"));
		
		String estado;
		if ((simular.getAnguloTensao() - simular.getAnguloCorrente()) < 0) estado=" adiantado";
		else if ((simular.getAnguloTensao() - simular.getAnguloCorrente())>0)  estado=" atrasado"; 
		else estado = "";
		
		//setText dos calculos do fator de potencia  e estado 
		textFieldFatorDePotencia.setText(String.valueOf(simular.calculaFatorDePotencia() + estado));
		
		//forma o grafico da PotenciaInstantanea
		scoresPotenciaInstantanea.clear();
	
		for (double i = 1.05; i <7.3325 ;  i =(double) i+0.001) {  
			scoresPotenciaInstantanea.add(simular.calculaFormaOndaPotenciaInstantanea(i));
		}
	
		graphPanelGraficoPotenciaInstantanea.revalidate();
		graphPanelGraficoPotenciaInstantanea.repaint();
		
		//forma o Triangulo de potencias, revalida e repinta todas as vezes que forem inseridos novos valores
		trianguloPotencias.revalidate(); 
		trianguloPotencias.repaint();
		
		trianguloPotencias.setCoordenadaX(simular.calculaPotenciaAtiva()/100); //potencia ativa e potencia reativa 
		trianguloPotencias.setCoordenadaY(simular.calculaPotenciaReativa()/100); //tiveram os valores divididos por 100
		                                                                       //apenas para o triangulo de potencias
	}                                                                          //nao ultrapassar os limetes do quadrado
}                                                                              //no grafico
