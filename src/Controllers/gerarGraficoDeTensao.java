package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Model.calculosSimulacaoFluxoPotenciaFundamental;
import View.GraficosSimulacaoFPF;

public class gerarGraficoDeTensao implements ActionListener {
	
	private calculosSimulacaoFluxoPotenciaFundamental tensao;
	private List<Double> scoresTensao;
	private GraficosSimulacaoFPF graficoTensao;
	private JTextField amplitudeTensao;
	private JTextField anguloTensao;
	
	
	public gerarGraficoDeTensao(List<Double> scoresTensao, GraficosSimulacaoFPF graficoTensao, JTextField amplitudeTensao, JTextField anguloTensao) {
		
		tensao = new calculosSimulacaoFluxoPotenciaFundamental();
		this.scoresTensao=scoresTensao;
		this.graficoTensao=graficoTensao;
		this.amplitudeTensao=amplitudeTensao;
		this.anguloTensao=anguloTensao;
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		tensao.setAmplituteTensao(Double.parseDouble(amplitudeTensao.getText()));	
		tensao.setAnguloTensao(Double.parseDouble(anguloTensao.getText()));	

		if(tensao.getAmplituteTensao() > 220 || tensao.getAmplituteTensao() < 0) {
			
			JOptionPane error = new JOptionPane("        Opera��o Inv�lida!!!", JOptionPane.ERROR_MESSAGE);
			JDialog errorDialog = error.createDialog("ERROR");
			errorDialog.setAlwaysOnTop(true); //JDialog criado para colocar JOptionPane tambem como setAlwaysOnTop(true)
			errorDialog.setVisible(true);	  
	
		}	
		else {
			scoresTensao.clear();//apaga os valores anteriores corrigindo para os novos valores digitados dos angulos e amplitudes para formar o grafico coretamente
			// valores i testados aleatoriamente at� darem certo kk
			for (double i = 1.05; i <7.3325 ;  i =(double) i + 0.001) {  
				
				scoresTensao.add(tensao.calculaFormaOndaTensao(i));
		
			}
      	
			graficoTensao.revalidate();
			graficoTensao.repaint();
		
		}
	}
}

