package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Model.calculosSimulacaoFluxoPotenciaFundamental;
import View.GraficosSimulacaoFPF;

public class gerarGraficoDeCorrente implements ActionListener {

	private calculosSimulacaoFluxoPotenciaFundamental corrente;	
	private List<Double> scoresCorrente;
	private GraficosSimulacaoFPF graphPanelGraficoCorrente;
	private JTextField textFieldAmplitudeIrms;
	private JTextField textFieldAnguloCorrente;
	
	public gerarGraficoDeCorrente(List<Double> scoresCorrente, GraficosSimulacaoFPF graphPanelGraficoCorrente, JTextField textFieldAmplitudeIrms, JTextField textFieldAnguloCorrente) {
		corrente = new calculosSimulacaoFluxoPotenciaFundamental();
		this.scoresCorrente=scoresCorrente;
		this.graphPanelGraficoCorrente=graphPanelGraficoCorrente;
		this.textFieldAmplitudeIrms=textFieldAmplitudeIrms;
		this.textFieldAnguloCorrente=textFieldAnguloCorrente;
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		corrente.setAmplitudeCorrente(Double.parseDouble(textFieldAmplitudeIrms.getText()));
		corrente.setAnguloCorrente(Double.parseDouble(textFieldAnguloCorrente.getText()));
		
		if(corrente.getAmplitudeCorrente() > 100 || corrente.getAmplitudeCorrente() < 0) {
			
			JOptionPane error = new JOptionPane("        Opera��o Inv�lida!!!", JOptionPane.ERROR_MESSAGE);
			JDialog errorDialog = error.createDialog("ERROR");
			errorDialog.setAlwaysOnTop(true); //JDialog criado para colocar JOptionPane tambem como setAlwaysOnTop(true)
			errorDialog.setVisible(true);	  
	
		}	
		else {
			scoresCorrente.clear();//apaga os valores anteriores corrigindo para os novos valores digitados dos angulos e amplitudes para formar o grafico coretamente
			// valores i testados aleatoriamente at� darem certo kk
			for (double i = 1.05; i <7.3325 ;  i =(double) i + 0.001) {  
				
				scoresCorrente.add(corrente.calculaFormaOndaCorrente(i));
		
			}
      	
			graphPanelGraficoCorrente.revalidate();
			graphPanelGraficoCorrente.repaint();
		
		}
	}
}
