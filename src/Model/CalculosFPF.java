package Model;

import java.util.Locale;

public interface CalculosFPF {
	
	public  double calculaFormaOndaTensao(double i);	
	public double calculaFormaOndaPotenciaInstantanea(double i);

}
