package Model;

import java.util.List;
import java.util.Locale;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import View.GraficosSimulacaoFPF;
import View.TrianguloDePotencias;

public class calculosSimulacaoFluxoPotenciaFundamental implements CalculosFPF  {
	
	private double amplituteTensao;
	private double anguloTensao;
	private double amplitudeCorrente;
	private double anguloCorrente;
	private double frequenciaAngular;

	public calculosSimulacaoFluxoPotenciaFundamental() {
		frequenciaAngular = 2*60*Math.PI;
	}

	public double getAmplituteTensao() {
		return amplituteTensao;
	}
	
	public void setAmplituteTensao(double amplituteTensao) {
		this.amplituteTensao = amplituteTensao;
	}

	public double getAnguloTensao() {
		return anguloTensao;
	}
	
	public void setAnguloTensao(double anguloTensao) {
		this.anguloTensao = anguloTensao;
	}

	public double getAmplitudeCorrente() {
		return amplitudeCorrente;
	}

	public void setAmplitudeCorrente(double amplitudeCorrente) {
		this.amplitudeCorrente = amplitudeCorrente;
	}

	public double getAnguloCorrente() {
		return anguloCorrente;
	}
	
	public void setAnguloCorrente(double anguloCorrente) {
		this.anguloCorrente = anguloCorrente;
	}

	public double calculaFormaOndaTensao(double i) {	
		double x =  2.19324542246;         // dividindo a frequencia angular por x apenas para que os graficos;
		double w = frequenciaAngular / x;  // de tensao fiquem com 3 ondas.
		return amplituteTensao * Math.cos(Math.toRadians(w*i + anguloTensao));
			 //amplituteTensao * cos(wt + anguloTensao);	
	}
	
	public double calculaFormaOndaCorrente(double i) {
		double x =  2.19324542246;        // dividindo a frequencia angular por x apenas para que os graficos;
		double w = frequenciaAngular / x; // de tensao fiquem com 3 ondas.	
		return amplitudeCorrente * Math.cos(Math.toRadians(w*i + anguloCorrente));
			 //amplituteTensao *      cos(          wt        + anguloTensao);
	}
	
	public double calculaFormaOndaPotenciaInstantanea(double i) {
		return calculaFormaOndaTensao(i) * calculaFormaOndaCorrente (i) ;		
	}
	
	public double calculaPotenciaAtiva() {
		double potenciaAtiva = amplituteTensao * amplitudeCorrente * Math.cos(anguloTensao - anguloCorrente);
		return potenciaAtiva;
	}
	
	public double calculaPotenciaReativa() {
		double potenciaReativa = amplituteTensao * amplitudeCorrente * Math.sin(anguloTensao - anguloCorrente);
		return potenciaReativa;
	}
	
	public double calculaPotenciaAparente() {
		double potenciaAparente = amplituteTensao * amplitudeCorrente;
		return potenciaAparente;
	}
	
	public double calculaFatorDePotencia() {
		double fatorDePotencia = Math.cos(anguloTensao - anguloCorrente);
		fatorDePotencia = Double.valueOf(String.format(Locale.US, "%.2f", fatorDePotencia));
		return fatorDePotencia;
	}
	
}