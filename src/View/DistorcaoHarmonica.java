package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controllers.voltarAoMenuInicial;


import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.CompoundBorder;
import javax.swing.JRadioButton;
import javax.swing.JFormattedTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JSplitPane;
import javax.swing.JScrollBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JInternalFrame;

public class DistorcaoHarmonica {
	private JFrame contentFrame;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	public DistorcaoHarmonica() {
		contentFrame = new JFrame();
		contentFrame.setTitle("Simula\u00E7\u00E3o da Distorc\u00E3o Harm\u00F4nica");
		contentFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentFrame.setBounds(100, 100, 800, 1512);
		
		contentFrame.setAlwaysOnTop(true);
		contentFrame.setVisible(true);
		contentPane = new JPanel();
		
		contentFrame.setContentPane(contentPane);
		contentFrame.setLocationRelativeTo(null);		
		contentPane.setLayout(null);
		
		List<Double> scoresComponent = new ArrayList<>();
		
		GraficosSimulacaoDH componente = new GraficosSimulacaoDH(scoresComponent, 0, 0);
		componente.setBounds(290, 0, 494, 191);
		contentPane.add(componente);
		componente.setLayout(null);
		
		List<Double> scoresRezulante = new ArrayList<>();
		
		GraficosSimulacaoDH rezultante = new GraficosSimulacaoDH(scoresRezulante, 0, 0);
		rezultante.setBounds(0, 542, 435, 191);
		contentPane.add(rezultante);
		rezultante.setLayout(null);
		
		JLabel lblNewLabel_3 = new JLabel("Gr\u00E1fico Rezultante");
		lblNewLabel_3.setForeground(Color.DARK_GRAY);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_3.setBounds(183, 166, 148, 14);
		rezultante.add(lblNewLabel_3);
		

		
		JLabel lblNewLabel_2 = new JLabel("Forma de Onda da Componente Fundamental");
		lblNewLabel_2.setForeground(Color.DARK_GRAY);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_2.setBounds(103, 166, 292, 14);
		componente.add(lblNewLabel_2);
		
		JLabel lblNumero = new JLabel("N\u00FAmero de harm\u00F4nicos");
		lblNumero.setBounds(62, 223, 135, 14);
		contentPane.add(lblNumero);
		
		JSpinner harmonicos = new JSpinner();
		harmonicos.setBounds(89, 247, 67, 20);
		harmonicos.setModel(new SpinnerNumberModel(1, 1, 6, 1));
		contentPane.add(harmonicos);
		
		JRadioButton impares = new JRadioButton("Impares");
		impares.setBounds(326, 245, 109, 23);
		contentPane.add(impares);
		
		JRadioButton pares = new JRadioButton("Pares");
		pares.setBounds(325, 222, 109, 23);
		contentPane.add(pares);
		
		JButton ok = new JButton("OK");
		ok.setBounds(565, 232, 77, 23);
		contentPane.add(ok);
		
		JButton btnNewButton = new JButton("VOLTAR AO MENU INICIAL");
		btnNewButton.addActionListener(new voltarAoMenuInicial(contentFrame));

		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnNewButton.setBounds(518, 687, 198, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("Amplitude (Vrms)");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setBounds(99, 24, 117, 14);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(109, 47, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Angulo de Fase (\u03B8v)");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_1.setBounds(100, 78, 156, 14);
		contentPane.add(lblNewLabel_1);
		
		textField_1 = new JTextField();
		textField_1.setBounds(108, 103, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton_1 = new JButton("GERAR ONDA");
		btnNewButton_1.setBounds(98, 134, 109, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("SIMULAR");
		btnNewButton_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnNewButton_2.setBounds(550, 653, 117, 23);
		contentPane.add(btnNewButton_2);
		
		JLabel lblSrieDeFourier = new JLabel("S\u00E9rie de Fourier Amplitude Fase");
		lblSrieDeFourier.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSrieDeFourier.setBounds(496, 542, 229, 14);
		contentPane.add(lblSrieDeFourier);
		
		Harmonicos panelHarmonicos = new Harmonicos();
		
		JScrollPane scrollPane = new JScrollPane(panelHarmonicos);
		panelHarmonicos.setLayout(null);
		
		scrollPane.setBounds(0, 278, 784, 253);
		contentPane.add(scrollPane);
		

		



	
		
		

	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DistorcaoHarmonica frame = new DistorcaoHarmonica();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
