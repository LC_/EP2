package View;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controllers.acaoDistorcaoHarmonica;
import Controllers.acaoFluxoDePotenciaFundamental;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MenuInicial {
	
	private JFrame frameTelaInicialAQEE;
	private JPanel panelTelaInicialAQEE;
	
	public MenuInicial() {
		
		frameTelaInicialAQEE = new JFrame();
		frameTelaInicialAQEE.setAlwaysOnTop(true);  // frame sempre aparece por cima
		frameTelaInicialAQEE.setVisible(true); 
		frameTelaInicialAQEE.setTitle("Aprenda Qualidade de Enegia El\u00E9trica"); //titulo do frame
		frameTelaInicialAQEE.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		frameTelaInicialAQEE.setBounds(100, 100, 465, 262); 
		panelTelaInicialAQEE = new JPanel();
		panelTelaInicialAQEE.setBorder(new EmptyBorder(5, 5, 5, 5));
		frameTelaInicialAQEE.getContentPane().add(panelTelaInicialAQEE);
		panelTelaInicialAQEE.setLayout(null);
		frameTelaInicialAQEE.setLocationRelativeTo(null); //frame sempre no centro
		
		JButton buttonFluxoPotenciaFundamental = new JButton("Fluxo de Pot\u00EAncia Fundamental");
		buttonFluxoPotenciaFundamental.setFocusPainted(false);
		buttonFluxoPotenciaFundamental.setBounds(124, 84, 212, 23);
		buttonFluxoPotenciaFundamental.addActionListener(new acaoFluxoDePotenciaFundamental(frameTelaInicialAQEE));
		panelTelaInicialAQEE.add(buttonFluxoPotenciaFundamental);

		JButton buttonDistorcaoHarmonica = new JButton("Distor\u00E7\u00E3o Harm\u00F4nica");
		buttonDistorcaoHarmonica.addActionListener(new acaoDistorcaoHarmonica(frameTelaInicialAQEE));
		buttonDistorcaoHarmonica.setBounds(124, 128, 210, 23);
		panelTelaInicialAQEE.add(buttonDistorcaoHarmonica);
		
		JLabel lblEscolhaOTipo = new JLabel("Escolha o tipo de simula\u00E7\u00E3o :");
		lblEscolhaOTipo.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblEscolhaOTipo.setBounds(60, 30, 194, 14);
		panelTelaInicialAQEE.add(lblEscolhaOTipo);
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuInicial frame = new MenuInicial();;
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}		
	
}