package View;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JSpinner;

public class Harmonicos extends JPanel {
	

	public Harmonicos() {
		JFrame frame = new JFrame();
		setMinimumSize(new Dimension(0, 0));
		
		setPreferredSize(new Dimension(765, 1512));
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 764, 1512);
		add(panel);
		panel.setLayout(new GridLayout(6, 1, 0, 0));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblAnguloDeFase = new JLabel("Angulo de fase");
		lblAnguloDeFase.setBounds(78, 21, 98, 14);
		panel_1.add(lblAnguloDeFase);
		
		JLabel lblAmplitude = new JLabel("Amplitude");
		lblAmplitude.setBounds(84, 87, 69, 14);
		panel_1.add(lblAmplitude);
		
		List<Double> scoresHarm1 = new ArrayList<>();
		
		GraficosSimulacaoDH harm1 = new GraficosSimulacaoDH(scoresHarm1, 0, 0);
		harm1.setBounds(215, 0, 600, 252);
		panel_1.add(harm1);
		JLabel lblNewLabel = new JLabel("Ordem Harmonica");
		lblNewLabel.setBounds(67, 154, 109, 14);
		panel_1.add(lblNewLabel);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(84, 46, 59, 20);
		panel_1.add(spinner);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setBounds(84, 112, 59, 20);
		panel_1.add(spinner_1);
		
		JSpinner spinner_2 = new JSpinner();
		spinner_2.setBounds(84, 179, 59, 20);
		panel_1.add(spinner_2);
		
		JPanel panel_2 = new JPanel();
		panel_2.setForeground(Color.GRAY);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		List<Double> scoresHarm2 = new ArrayList<>();
		
		GraficosSimulacaoDH harm2 = new GraficosSimulacaoDH(scoresHarm2,0,0);
		harm2.setBounds(215, 0, 600, 252);
		panel_2.add(harm2);
		
		JSpinner spinner_3 = new JSpinner();
		spinner_3.setBounds(87, 193, 59, 20);
		panel_2.add(spinner_3);
		
		JLabel label = new JLabel("Ordem Harmonica");
		label.setBounds(70, 168, 109, 14);
		panel_2.add(label);
		
		JSpinner spinner_4 = new JSpinner();
		spinner_4.setBounds(87, 126, 59, 20);
		panel_2.add(spinner_4);
		
		JLabel label_5 = new JLabel("Amplitude");
		label_5.setBounds(87, 101, 69, 14);
		panel_2.add(label_5);
		
		JSpinner spinner_5 = new JSpinner();
		spinner_5.setBounds(87, 60, 59, 20);
		panel_2.add(spinner_5);
		
		JLabel label_10 = new JLabel("Angulo de fase");
		label_10.setBounds(81, 35, 98, 14);
		panel_2.add(label_10);
		
		JPanel panel_3 = new JPanel();
		panel.add(panel_3);
		panel_3.setLayout(null);
		
		List<Double> scoresHarm3 = new ArrayList<>();
		GraficosSimulacaoDH harm3 = new GraficosSimulacaoDH(scoresHarm3, 0, 0);
		harm3.setBounds(215, 0, 600, 252);
		panel_3.add(harm3);
		
		JSpinner spinner_6 = new JSpinner();
		spinner_6.setBounds(86, 188, 59, 20);
		panel_3.add(spinner_6);
		
		JLabel label_1 = new JLabel("Ordem Harmonica");
		label_1.setBounds(69, 163, 109, 14);
		panel_3.add(label_1);
		
		JSpinner spinner_7 = new JSpinner();
		spinner_7.setBounds(87, 126, 59, 20);
		panel_3.add(spinner_7);
		
		JLabel label_6 = new JLabel("Amplitude");
		label_6.setBounds(87, 101, 69, 14);
		panel_3.add(label_6);
		
		JSpinner spinner_8 = new JSpinner();
		spinner_8.setBounds(86, 55, 59, 20);
		panel_3.add(spinner_8);
		
		JLabel label_11 = new JLabel("Angulo de fase");
		label_11.setBounds(81, 35, 98, 14);
		panel_3.add(label_11);
		
		JPanel panel_4 = new JPanel();
		panel.add(panel_4);
		panel_4.setLayout(null);
		
		JSpinner spinner_9 = new JSpinner();
		spinner_9.setBounds(87, 60, 59, 20);
		panel_4.add(spinner_9);
		
		JSpinner spinner_10 = new JSpinner();
		spinner_10.setBounds(87, 126, 59, 20);
		panel_4.add(spinner_10);
		
		JSpinner spinner_11 = new JSpinner();
		spinner_11.setBounds(84, 185, 59, 20);
		panel_4.add(spinner_11);
		
		JLabel label_2 = new JLabel("Angulo de fase");
		label_2.setBounds(81, 35, 98, 14);
		panel_4.add(label_2);
		
		JLabel label_7 = new JLabel("Amplitude");
		label_7.setBounds(87, 101, 69, 14);
		panel_4.add(label_7);
		
		JLabel label_12 = new JLabel("Ordem Harmonica");
		label_12.setBounds(70, 168, 109, 14);
		panel_4.add(label_12);
		
		List<Double> scoresHarm4 = new ArrayList<>();
		GraficosSimulacaoDH harm4 = new GraficosSimulacaoDH(scoresHarm4, 0, 0);
		harm4.setBounds(215, 0, 600, 252);
		panel_4.add(harm4);
		
		JPanel panel_5 = new JPanel();
		panel.add(panel_5);
		panel_5.setLayout(null);
		
		JSpinner spinner_12 = new JSpinner();
		spinner_12.setBounds(87, 60, 59, 20);
		panel_5.add(spinner_12);
		
		JSpinner spinner_13 = new JSpinner();
		spinner_13.setBounds(87, 126, 59, 20);
		panel_5.add(spinner_13);
		
		JSpinner spinner_14 = new JSpinner();
		spinner_14.setBounds(84, 185, 59, 20);
		panel_5.add(spinner_14);
		
		JLabel label_3 = new JLabel("Angulo de fase");
		label_3.setBounds(81, 35, 98, 14);
		panel_5.add(label_3);
		
		JLabel label_8 = new JLabel("Amplitude");
		label_8.setBounds(87, 101, 69, 14);
		panel_5.add(label_8);
		
		JLabel label_13 = new JLabel("Ordem Harmonica");
		label_13.setBounds(70, 168, 109, 14);
		panel_5.add(label_13);
		
		List<Double> scoresHarm5 = new ArrayList<>();
		
		GraficosSimulacaoDH harm5 = new GraficosSimulacaoDH(scoresHarm5, 0, 0);
		harm5.setBounds(215, 0, 600, 252);
		panel_5.add(harm5);
		
		JPanel panel_6 = new JPanel();
		panel.add(panel_6);
		panel_6.setLayout(null);
		
		JSpinner spinner_15 = new JSpinner();
		spinner_15.setBounds(87, 60, 59, 20);
		panel_6.add(spinner_15);
		
		JSpinner spinner_16 = new JSpinner();
		spinner_16.setBounds(87, 126, 59, 20);
		panel_6.add(spinner_16);
		
		JSpinner spinner_17 = new JSpinner();
		spinner_17.setBounds(84, 185, 59, 20);
		panel_6.add(spinner_17);
		
		JLabel label_4 = new JLabel("Angulo de fase");
		label_4.setBounds(81, 35, 98, 14);
		panel_6.add(label_4);
		
		JLabel label_9 = new JLabel("Amplitude");
		label_9.setBounds(87, 101, 69, 14);
		panel_6.add(label_9);
		
		JLabel label_14 = new JLabel("Ordem Harmonica");
		label_14.setBounds(70, 168, 109, 14);
		panel_6.add(label_14);
		
		List<Double> scoresHarm6 = new ArrayList<>();
		GraficosSimulacaoDH harm6 = new GraficosSimulacaoDH(scoresHarm6, 0, 0);
		harm6.setBounds(215, 0, 600, 252);
		panel_6.add(harm6);
		//setPreferredSize(new Dimension(764, 1512));
		
		
		

		
		
	}
	public Dimension getThisPreferredSize() {
		return getPreferredSize();
	}
	public void setThisPreferredSize(Dimension preferredSize) {
		setPreferredSize(preferredSize);
	}
}
