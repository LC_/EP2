package View;
import javax.swing.JFrame;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controllers.gerarGraficoDeCorrente;
import Controllers.gerarGraficoDeTensao;
import Controllers.simularFluxoDePotenciaFundamental;
import Controllers.voltarAoMenuInicial;

import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FluxoPotenciaFundamental {
	private JFrame frameFluxoPotenciaFundamental;
	private JPanel panelFluxoPotenciaFundamental;
	private JTextField amplitudeTensao;
	private JTextField anguloTensao;
	private JTextField anguloCorrente;
	private JTextField potenciaAtiva;
	private JTextField potenciaReativa;
	private JTextField potenciaAparente;
	private JTextField fatorDePotencia;
	private JTextField amplitudeCorrente;
	private GraficosSimulacaoFPF graficoCorrente;

	public FluxoPotenciaFundamental() {
		frameFluxoPotenciaFundamental = new JFrame();
		frameFluxoPotenciaFundamental.setResizable(false);
		frameFluxoPotenciaFundamental.setAlwaysOnTop(true);
		frameFluxoPotenciaFundamental.setVisible(true);
		frameFluxoPotenciaFundamental.setTitle("Simula\u00E7\u00E3o do Fluxo de Pot\u00EAncia Fundamental");
		
		frameFluxoPotenciaFundamental.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameFluxoPotenciaFundamental.setBounds(100, 100, 800, 775);
		panelFluxoPotenciaFundamental = new JPanel();
		panelFluxoPotenciaFundamental.setBorder(new EmptyBorder(5, 5, 5, 5));
		frameFluxoPotenciaFundamental.getContentPane().add(panelFluxoPotenciaFundamental);
		panelFluxoPotenciaFundamental.setLayout(null);
		frameFluxoPotenciaFundamental.setLocationRelativeTo(null);
		
		JLabel labelAmplitudeTensao = new JLabel("Amplitude (Vrms)");
		labelAmplitudeTensao.setFont(new Font("Tahoma", Font.BOLD, 12));
		labelAmplitudeTensao.setBounds(48, 32, 119, 20);
		panelFluxoPotenciaFundamental.add(labelAmplitudeTensao);
		
		amplitudeTensao = new JTextField();
		amplitudeTensao.setBounds(48, 55, 104, 20);
		panelFluxoPotenciaFundamental.add(amplitudeTensao);
		amplitudeTensao.setColumns(10);				
		amplitudeTensao.setText(String.valueOf(0));//amplitude inicia com 0
		
		JLabel labelAnguloTensao = new JLabel("<html>\u00C2ngulo de Fase <br>da Tens\u00E3o (\u03B8v)</html>");
		labelAnguloTensao.setFont(new Font("Tahoma", Font.BOLD, 13));
		labelAnguloTensao.setBounds(48, 73, 119, 41);
		panelFluxoPotenciaFundamental.add(labelAnguloTensao);
		
		anguloTensao = new JTextField();
		anguloTensao.setBounds(48, 116, 104, 20);
		panelFluxoPotenciaFundamental.add(anguloTensao);
		anguloTensao.setColumns(10);
		anguloTensao.setText(String.valueOf(0));//angulo inicia com 0
		JButton gerarGraficoTensao = new JButton("Gerar Onda");
	
		gerarGraficoTensao.setBounds(186, 72, 104, 30);
		panelFluxoPotenciaFundamental.add(gerarGraficoTensao);
		
		
		JLabel labelAmplitudeCorrente = new JLabel("Amplitude (Irms)");
		labelAmplitudeCorrente.setFont(new Font("Tahoma", Font.BOLD, 12));
		labelAmplitudeCorrente.setBounds(48, 222, 105, 14);
		panelFluxoPotenciaFundamental.add(labelAmplitudeCorrente);
		
		amplitudeCorrente = new JTextField();
		amplitudeCorrente.setBounds(48, 240, 104, 20);
		panelFluxoPotenciaFundamental.add(amplitudeCorrente);
		amplitudeCorrente.setColumns(10);
		amplitudeCorrente.setText(String.valueOf(0));//amplitude inicia com 0
		
		JLabel labelAnguloCorrente = new JLabel("<html>\u00C2ngulo de Fase <br>da Corrente (\u03B8i)</html>");
		labelAnguloCorrente.setFont(new Font("Tahoma", Font.BOLD, 12));
		labelAnguloCorrente.setBounds(48, 259, 119, 41);
		panelFluxoPotenciaFundamental.add(labelAnguloCorrente);
		
		anguloCorrente = new JTextField();
		anguloCorrente.setBounds(48, 295, 104, 20);
		panelFluxoPotenciaFundamental.add(anguloCorrente);
		anguloCorrente.setColumns(10);
		anguloCorrente.setText(String.valueOf(0));//angulo inicia com 0
		JButton gerarGraficoCorrente = new JButton("Gerar Onda");
		
		gerarGraficoCorrente.setBounds(186, 263, 104, 30);
		panelFluxoPotenciaFundamental.add(gerarGraficoCorrente);
    	
    	List<Double> scoresTensao = new ArrayList<>();
		
		GraficosSimulacaoFPF graficoTensao = new GraficosSimulacaoFPF(scoresTensao, 0, 0);
		graficoTensao.setBounds(290, 0, 504, 191);
		panelFluxoPotenciaFundamental.add(graficoTensao);
		graficoTensao.setLayout(null);
		panelFluxoPotenciaFundamental.repaint();
		
		gerarGraficoTensao.addActionListener(new gerarGraficoDeTensao(scoresTensao, graficoTensao, amplitudeTensao, anguloTensao));
		
		JLabel lblFormaDeOnda = new JLabel("Forma de Onda da Tens\u00E3o");
		lblFormaDeOnda.setForeground(Color.DARK_GRAY);
		lblFormaDeOnda.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblFormaDeOnda.setBounds(178, 166, 176, 14);
		graficoTensao.add(lblFormaDeOnda);
		
		List<Double> scoresCorrente = new ArrayList<>();
		graficoCorrente = new GraficosSimulacaoFPF(scoresCorrente, 0, 0);//graficos comecam com amplitude 0 e angulo 0
		graficoCorrente.setBounds(290, 190, 504, 191);
		panelFluxoPotenciaFundamental.add(graficoCorrente);
		graficoCorrente.setLayout(null);
		
		gerarGraficoCorrente.addActionListener(new gerarGraficoDeCorrente(scoresCorrente, graficoCorrente, amplitudeCorrente, anguloCorrente));

		JLabel lblFormaDeOnda_1 = new JLabel("Forma de Onda da Corrente");
		lblFormaDeOnda_1.setForeground(Color.DARK_GRAY);
		lblFormaDeOnda_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblFormaDeOnda_1.setBounds(178, 166, 183, 14);
		graficoCorrente.add(lblFormaDeOnda_1);
		graficoCorrente.repaint();
		
		List<Double> scoresPotenciaInstantanea = new ArrayList<>();
		
		GraficosSimulacaoFPF graficoPotenciaInstantanea = new GraficosSimulacaoFPF(scoresPotenciaInstantanea, 0, 0);
		graficoPotenciaInstantanea.setBounds(290, 379, 504, 191);
		panelFluxoPotenciaFundamental.add(graficoPotenciaInstantanea);
		graficoPotenciaInstantanea.setLayout(null);
		
		JLabel lblFormaDeOnda_2 = new JLabel("Forma de Onda da Pot\u00EAncia Instant\u00E2nea ");
		lblFormaDeOnda_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblFormaDeOnda_2.setForeground(Color.DARK_GRAY);
		lblFormaDeOnda_2.setBounds(149, 166, 268, 14);
		graficoPotenciaInstantanea.add(lblFormaDeOnda_2);
		
		JLabel labelPotenciaAtiva = new JLabel("Potencia Ativa");
		labelPotenciaAtiva.setBounds(375, 587, 104, 14);
		panelFluxoPotenciaFundamental.add(labelPotenciaAtiva);
		
		potenciaAtiva = new JTextField();
		potenciaAtiva.setBounds(489, 584, 86, 20);
		panelFluxoPotenciaFundamental.add(potenciaAtiva);
		potenciaAtiva.setColumns(10);
		
		JLabel labelPotenciaReativa = new JLabel("Potencia Reativa");
		labelPotenciaReativa.setBounds(374, 612, 105, 14);
		panelFluxoPotenciaFundamental.add(labelPotenciaReativa);
		
		potenciaReativa = new JTextField();
		potenciaReativa.setBounds(489, 609, 86, 20);
		panelFluxoPotenciaFundamental.add(potenciaReativa);
		potenciaReativa.setColumns(10);
		
		JLabel labelPotenciaAparente = new JLabel("Potencia Aparente");
		labelPotenciaAparente.setBounds(374, 637, 105, 14);
		panelFluxoPotenciaFundamental.add(labelPotenciaAparente);
		
		potenciaAparente = new JTextField();
		potenciaAparente.setBounds(489, 634, 86, 20);
		panelFluxoPotenciaFundamental.add(potenciaAparente);
		potenciaAparente.setColumns(10);
		
		JLabel labelFatorDePotencia = new JLabel("Fator de Potencia");
		labelFatorDePotencia.setBounds(374, 662, 105, 14);
		panelFluxoPotenciaFundamental.add(labelFatorDePotencia);
		
		fatorDePotencia = new JTextField();
		fatorDePotencia.setBounds(489, 659, 86, 20);
		panelFluxoPotenciaFundamental.add(fatorDePotencia);
		fatorDePotencia.setColumns(10);
		
		JLabel labelTringuloDePotencias = new JLabel("Tri\u00E2ngulo de Pot\u00EAncias");
		labelTringuloDePotencias.setFont(new Font("Tahoma", Font.BOLD, 13));
		labelTringuloDePotencias.setBounds(76, 368, 160, 20);
		panelFluxoPotenciaFundamental.add(labelTringuloDePotencias);
	
		JButton simularFluxoPotenciaFundamental = new JButton("SIMULAR");
		//float valor = Float.parseFloat(meujtextfield.getText());??
		simularFluxoPotenciaFundamental.setFont(new Font("Tahoma", Font.BOLD, 13));
		simularFluxoPotenciaFundamental.setBounds(650, 614, 104, 23);
		panelFluxoPotenciaFundamental.add(simularFluxoPotenciaFundamental);
		
		JButton voltarAoMenuInicial = new JButton("VOLTAR AO MENU INICIAL");

		voltarAoMenuInicial.addActionListener(new voltarAoMenuInicial(frameFluxoPotenciaFundamental)); 
		voltarAoMenuInicial.setFont(new Font("Verdana", Font.BOLD, 13));
		voltarAoMenuInicial.setBounds(290, 696, 228, 23);
		panelFluxoPotenciaFundamental.add(voltarAoMenuInicial);
		
		TrianguloDePotencias trianguloPotencias = new TrianguloDePotencias(0 , 0);
		trianguloPotencias.setSize(280, 280);
		trianguloPotencias.setLocation(10, 392);
		panelFluxoPotenciaFundamental.add(trianguloPotencias); 
		panelFluxoPotenciaFundamental.repaint();
		simularFluxoPotenciaFundamental.addActionListener(new simularFluxoDePotenciaFundamental( scoresPotenciaInstantanea, graficoPotenciaInstantanea,trianguloPotencias, amplitudeTensao, anguloTensao, amplitudeCorrente, anguloCorrente, potenciaAtiva, potenciaReativa, potenciaAparente, fatorDePotencia));
		
		JLabel labelPAt = new JLabel("Potencia Ativa");
		labelPAt.setForeground(Color.RED);
		labelPAt.setBounds(105, 677, 104, 14);
		panelFluxoPotenciaFundamental.add(labelPAt);
		
		JLabel labelPR = new JLabel("Potencia Reativa");
		labelPR.setForeground(Color.BLACK);
		labelPR.setBounds(105, 690, 104, 14);
		panelFluxoPotenciaFundamental.add(labelPR);
		
		JLabel labelPAp = new JLabel("Potencia Aparente");
		labelPAp.setForeground(Color.BLUE);
		labelPAp.setBounds(105, 702, 119, 14);
		panelFluxoPotenciaFundamental.add(labelPAp);
		
	}
	
	public static void main(String[] args) {
		FluxoPotenciaFundamental frame = new FluxoPotenciaFundamental();	
			
	}
}
